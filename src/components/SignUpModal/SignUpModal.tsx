import {
    Button,
    chakra,
    FormControl,
    FormLabel,
    Input,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    useToast,
} from '@chakra-ui/react';
import Script from 'next/script';
import { useRef, useState } from 'react';

export default function SignUpModal({ isOpen, onClose }: { isOpen: boolean; onClose: () => any }) {
    const toast = useToast();
    const initialRef = useRef<any>();
    const finalRef = useRef<any>();

    const [inputVals, setInputVals] = useState({ name: '', email: '' });
    // const [inputVals, setInputVals] = useState({
    //     name: 'test',
    //     email: 'testing@gmail.com',
    // });

    const [isSubmitting, setIsSubmitting] = useState(false);

    function onSubmit(event: any) {
        try {
            event.preventDefault();
            setIsSubmitting(true);
            setTimeout(() => {
                setIsSubmitting(false);
                onClose();
                toast({
                    title: 'Subscription success.',
                    description: 'Be on the look out in your inbox for further details.',
                    status: 'success',
                    variant: 'left-accent',
                    position: 'bottom-right',
                    isClosable: true,
                });
            }, 1000);
        } catch (error) {
            toast({
                title: 'Subscription failed.',
                description: 'Try again (or reach out to us on Discord).',
                status: 'error',
                variant: 'left-accent',
                position: 'bottom-right',
                isClosable: true,
            });
        }
    }

    return (
        <Modal initialFocusRef={initialRef} finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose} isCentered>
            <ModalOverlay />
            <ModalContent>
                <Script async src="https://f.convertkit.com/ckjs/ck.5.js" />

                <chakra.form
                    action="https://app.convertkit.com/forms/2496786/subscriptions"
                    method="post"
                    data-sv-form="2496786"
                    data-uid="f15adf77ac"
                    data-format="inline"
                    data-version="5"
                    onSubmit={onSubmit}
                >
                    <ModalHeader>Subscribe</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody pb={6}>
                        <FormControl>
                            <FormLabel>First name</FormLabel>
                            <Input
                                ref={initialRef}
                                placeholder="First name"
                                name="fields[first_name]"
                                aria-label="First name"
                                type="text"
                                value={inputVals.name}
                                onChange={(event: any) => setInputVals({ ...inputVals, name: event.target.value })}
                                isRequired
                                min={1}
                                maxLength={80}
                                focusBorderColor="lavender.400"
                            />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Email address</FormLabel>
                            <Input
                                placeholder="Email address"
                                name="email_address"
                                aria-label="Email address"
                                type="email"
                                value={inputVals.email}
                                onChange={(event: any) => setInputVals({ ...inputVals, email: event.target.value })}
                                isRequired
                                min={1}
                                maxLength={80}
                                focusBorderColor="lavender.400"
                            />
                        </FormControl>
                    </ModalBody>

                    <ModalFooter>
                        <Button
                            aria-label="Subscribe"
                            type="submit"
                            isDisabled={!inputVals.name || !inputVals.email}
                            isLoading={isSubmitting}
                            loadingText="Submitting"
                        >
                            Confirm
                        </Button>
                    </ModalFooter>
                </chakra.form>
            </ModalContent>
        </Modal>
    );
}
