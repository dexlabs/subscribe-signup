import { ChakraProvider } from '@chakra-ui/react';
import type { AppProps } from 'next/app';
import Head from 'next/head';

import theme from '../theme';

export default function MyApp({ Component, pageProps }: AppProps): any {
    return (
        <>
            <Head>
                <title>DerivaDEX | Amoeba</title>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover"
                />
                <link rel="shortcut icon" href="./favicon.ico" />
            </Head>
            <ChakraProvider theme={theme}>
                <Component {...pageProps} />
            </ChakraProvider>
        </>
    );
}
