import { Box, Button, Center, DarkMode, Heading, Image, Text, useDisclosure, VStack } from '@chakra-ui/react';

import SignUpModal from '../components/SignUpModal';

export default function Splash(): any {
    const { isOpen, onOpen, onClose } = useDisclosure();

    return (
        <DarkMode>
            <Box
                as="section"
                bg="gray.800"
                py="12"
                position="relative"
                bgImage="url(splash.webp)"
                bgSize="cover"
                bgPosition="center"
                h="100vh"
            >
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                    h="full"
                    zIndex={1}
                    position="relative"
                >
                    <Center flexDirection="column" textAlign="center" color="white" h="full">
                        <Heading
                            lineHeight="clamp(2rem,  6.25vw, 5.625rem)"
                            fontSize="clamp(2rem, 5.55vw, 5rem)"
                            fontWeight="200"
                            fontFamily="./TFN-Light-new.woff2"
                        >
                            GEN 2
                        </Heading>

                        <Text
                            lineHeight="clamp(3rem,  6.95vw, 6.25rem)"
                            mt="1.5rem"
                            fontSize="clamp(6rem, 14.5vw, 13.125rem)"
                            fontWeight="700"
                            fontFamily="./TFN-DemiBold.woff2"
                        >
                            AMOEBA
                        </Text>
                        <Button mt="3rem" size="lg" onClick={onOpen}>
                            Subscribe!
                        </Button>
                    </Center>
                </Box>
                <Box position="absolute" w="full" bottom="4" py="4">
                    <Box mx="auto" color="white">
                        <Box position="absolute" w="full" bottom="0">
                            <VStack mx="auto">
                                <Text fontSize="clamp(1.5rem, 2.22vw, 2rem)" fontFamily="./TFN-Medium.woff2">
                                    An invite-only trading competition on
                                </Text>
                                <Image src="./logo.webp" alt="DerivaDEX logo" w="clamp(300px, 27.78vw, 400px)" />
                            </VStack>
                        </Box>
                    </Box>
                </Box>
            </Box>
            <SignUpModal isOpen={isOpen} onClose={onClose} />
        </DarkMode>
    );
}
