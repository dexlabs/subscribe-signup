import Document, { Head, Html, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
    static async getInitialProps(ctx: any) {
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps };
    }

    render() {
        return (
            <Html lang="en">
                <Head>
                    <meta name="description" content="DerivaDEX Amoeba" />
                    <meta name="theme-color" content="#060a24" />
                    <link rel="icon" type="image/png" href="/favicon.ico" />
                    <link rel="apple-touch-icon" href="/favicon.ico" />

                    <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="robots" content="index, follow" />
                    <meta key="googlebot" name="googlebot" content="index,follow" />
                    <meta name="mobile-web-app-capable" content="yes" />
                    <meta name="apple-mobile-web-app-capable" content="yes" />
                    <meta name="keywords" content="DerivaDEX, cryptocurrency exchange, perpetual swaps, ethereum" />
                    <meta property="og:locale" content="en_US" />
                    {/* Facebook */}
                    <meta property="og:site_name" content="DerivaDEX Amoeba" />
                    <meta property="og:title" content="DerivaDEX Amoeba" />
                    <meta property="og:description" content="DerivaDEX Amoeba" />
                    <meta property="og:url" content="https://www.derivadex.com/subscribe" />
                    <meta property="og:image" content="https://www.derivadex.com/leaderboard/images/logo.png" />
                    {/* Twitter */}
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:site" content="@DDX_Official" />
                    <meta property="twitter:title" content="DerivaDEX Amoeba" />
                    <meta property="twitter:description" content="DerivaDEX Amoeba" />
                    <meta property="twitter:url" content="https://www.derivadex.com/subscribe" />
                    <meta property="twitter:image" content="https://www.derivadex.com/leaderboard/images/logo.png" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
